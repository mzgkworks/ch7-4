//
//  DetailViewController.swift
//  Animals
//
//  Created by mzgk on 2018/02/16.
//  Copyright © 2018年 mzgk. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {


    // MARK: - Outlet
    @IBOutlet weak var label: UILabel!

    // MARK: - Property
    var info: AnimalInfo!

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = info.name
        label.text = info.description
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
