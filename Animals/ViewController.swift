//
//  ViewController.swift
//  Animals
//
//  Created by mzgk on 2018/02/16.
//  Copyright © 2018年 mzgk. All rights reserved.
//

import UIKit

struct AnimalInfo {
    var name: String
    var description: String
}

// セルの操作はしないので、UITableViewDelegateには準拠不要
class ViewController: UIViewController, UITableViewDataSource {

    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!

    // MARK: Property
    let items = [
        AnimalInfo(name: "ライオン", description: "百獣の王。一般的に最も強い動物として知られている。"),
        AnimalInfo(name: "サイ", description: "頭部に硬い角を持っている。巨体に似合わず最高時速50kmで走る。"),
        AnimalInfo(name: "シマウマ", description: "白黒の縞模様を持つ動物。視覚や嗅覚、聴覚が優れている。"),
        AnimalInfo(name: "キリン", description: "もっとも背が高い動物。首が長いところが特徴。"),
        AnimalInfo(name: "ゾウ", description: "陸生動物では世界最大の動物。鼻は立っていても地面に届くほどに長い。")
    ]

    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // 画面遷移
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // どのセルがタップされたか（indexPathForSelectedRowがOptionalなのでオプショナルバインディングを利用）
        if let selectedRow = tableView.indexPathForSelectedRow {
            // segue.destinationには遷移先のVCが格納されている。型はUIViewControllerなので、遷移先のDetailViewController型にダウンキャストする
            let controller = segue.destination as! DetailViewController
            controller.info = items[selectedRow.row]
        }
    }

    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    // セルを生成：Identifier=NameCellを生成し、AnimalInfoの配列からCellの位置に対応する情報を取得。セルのデフォルトラベルに設定。
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // IndexPathごとにセルを使い分けないので、dequeueReusableCell(withIdentifier:)を使う
        let cell: UITableViewCell! = tableView.dequeueReusableCell(withIdentifier: "NameCell")
        let item = items[indexPath.row]
        cell.textLabel?.text = item.name
        return cell
    }
}

